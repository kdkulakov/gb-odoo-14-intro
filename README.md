# gb-odoo-14-intro

Проект для старта изучения фрээмворка Odoo с докером и плюшками..

## Запуск сервера
Для запуска сервера должен быть устанволен Docker и docker-compose

А дальше все просто
``docker-compose up``

Docker поднимит базу данных postgres и контейнер с Odoo 14.

В папке `addons` создаем своим модули.

Если нужно остановить контейнеры или перезапустить `docker-compose stop`

После запуска сервера в консоле должна появится такая надпись
``` ERROR ? odoo.modules.loading: Database odoo not initialized, you can force it with `-i base` ```
Она значит, что сервер odoo видит сервер баз данных и что база пуста, для ее инициализации нужно запустить сервер с ключом `-i base`

Для этого идем в `docker-compose.yml` и в строке `    command: odoo --dev=all,xml` добавлеям `-i base`.
Вот так должно получится `command: odoo --dev=all,xml -i base` (после инициализации базы вернуть как было)

После этого перезапускаем все `docker-compose down && docker-compose up`
Должен произойти процесс инициализации базы.

Этапом завершения инициализации базы должна быть строка в консоле `INFO odoo odoo.modules.loading: Modules loaded. `

Теперь можно зайти на страничку `http://127.0.0.1:8069` ввести логи|пароль - `admin|admin`.
Ну и в разделе `Apps` установить любые желаемые модули. И продолжить погружение в Odoo :)


Официальная документация ODOO 14

https://www.odoo.com/documentation/14.0/index.html